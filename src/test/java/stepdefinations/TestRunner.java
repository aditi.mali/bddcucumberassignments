package stepdefinations;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/Features/Login.feature",glue={"stepdefinations"})
public class TestRunner extends AbstractTestNGCucumberTests 
{

}
